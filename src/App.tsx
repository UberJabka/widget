import React, { useState } from 'react';
import AverageGraph from './components/AverageGraph';
import MarksList from './components/MarksList';
import RatingsFilter from './components/RatingFilter';
import Filters from './components/Filters';
import ReviewsList from './components/ReviewsList';
import styled from 'styled-components';
import BlockLabel from './components/BlockLabel';
import Button from './components/Button';
import { ReactComponent as Mic } from './assets/icons/microphone.svg';
import { useMediaQuery } from 'react-responsive';
import AccordeonBlock from './components/AccordeonBlock';
import Form from './components/Form';
const AppContainer = styled.div`
    font-family: 'Roboto', sans-serif;
    max-width: 1140px;
    margin: 0 auto;
`;

const Header = styled.div`
    display: flex;
    & > div {
        display: inline-block;
        flex: 0 0 25%;
        max-width: 25%;
        padding: 0 15px;
    }
    @media (max-width: 991px) {
        flex-wrap: wrap;
        & > div {
            flex: 0 0 50%;
            max-width: 50%;
            padding: 0;
        }
    }
    @media (max-width: 767px) {
        flex-direction: column;
        & > div {
            flex: 0 0 100%;
            max-width: 100%;
        }
    }
`;

const Wrapper = styled.div`
    flex: 1;
`;

const SplittedLabel = styled.span`
    justify-content: center;
    display: flex;
    align-items: center;
    height: 100%;
`;

const TextLabel = styled(SplittedLabel)`
    flex: 3;
`;

const IconLabel = styled(SplittedLabel)`
    flex: 1;
    background-color: #725aff;
    border-radius: 5px;
`;

const App = () => {
    const [isFormVisible, setVisibleForm] = useState(false);
    const isMobile = useMediaQuery({ maxWidth: 767 });

    return (
        <AppContainer>
            <Header>
                <AverageGraph />
                <Wrapper>
                    <BlockLabel title={'Добавить свой отзыв'} />
                    <Button
                        action={() =>
                            setVisibleForm((prevState) => !prevState)
                        }>
                        <TextLabel>Написать отзыв</TextLabel>
                        <IconLabel>
                            <Mic height={20} />
                        </IconLabel>
                    </Button>
                </Wrapper>
                {isMobile && (
                    <AccordeonBlock active={isFormVisible}>
                        <Form />
                    </AccordeonBlock>
                )}
                <MarksList />
                <RatingsFilter />
            </Header>
            {!isMobile && (
                <AccordeonBlock active={isFormVisible}>
                    <Form />
                </AccordeonBlock>
            )}
            <Filters />
            <ReviewsList />
        </AppContainer>
    );
};

export default App;
