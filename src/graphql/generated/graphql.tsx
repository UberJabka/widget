/* eslint-disable */
import gql from 'graphql-tag';
import * as ApolloReactCommon from '@apollo/client';
import * as ApolloReactHooks from '@apollo/client';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: any }> = { [K in keyof T]: T[K] };

/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /** The `Upload` scalar type represents a file upload. */
  Upload: any;
};

export type Query = {
  __typename?: 'Query';
  reviews: ReviewConnection;
  replies: ReplyConnection;
  /** Fetches an object given its ID */
  node?: Maybe<Node>;
};


export type QueryReviewsArgs = {
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  before?: Maybe<Scalars['String']>;
  last?: Maybe<Scalars['Int']>;
};


export type QueryRepliesArgs = {
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  before?: Maybe<Scalars['String']>;
  last?: Maybe<Scalars['Int']>;
};


export type QueryNodeArgs = {
  id: Scalars['ID'];
};

/** A connection to a list of items. */
export type ReviewConnection = {
  __typename?: 'ReviewConnection';
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<ReviewEdge>>>;
};

/** Information about pagination in a connection. */
export type PageInfo = {
  __typename?: 'PageInfo';
  /** When paginating forwards, are there more items? */
  hasNextPage: Scalars['Boolean'];
  /** When paginating backwards, are there more items? */
  hasPreviousPage: Scalars['Boolean'];
  /** When paginating backwards, the cursor to continue. */
  startCursor?: Maybe<Scalars['String']>;
  /** When paginating forwards, the cursor to continue. */
  endCursor?: Maybe<Scalars['String']>;
};

/** An edge in a connection. */
export type ReviewEdge = {
  __typename?: 'ReviewEdge';
  /** The item at the end of the edge */
  node?: Maybe<Review>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

/** A single review object */
export type Review = Node & {
  __typename?: 'Review';
  /** The ID of an object */
  id: Scalars['ID'];
  /** The main review text */
  value?: Maybe<Scalars['String']>;
  pros?: Maybe<Scalars['String']>;
  cons?: Maybe<Scalars['String']>;
  author: Author;
  rating: Scalars['Int'];
  date?: Maybe<Scalars['String']>;
};

/** An object with an ID */
export type Node = {
  /** The id of the object. */
  id: Scalars['ID'];
};

export type Author = {
  __typename?: 'Author';
  name: Scalars['String'];
  photo?: Maybe<Image>;
  location?: Maybe<Scalars['String']>;
};

export type Image = {
  __typename?: 'Image';
  url: Scalars['String'];
};

/** A connection to a list of items. */
export type ReplyConnection = {
  __typename?: 'ReplyConnection';
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<ReplyEdge>>>;
};

/** An edge in a connection. */
export type ReplyEdge = {
  __typename?: 'ReplyEdge';
  /** The item at the end of the edge */
  node?: Maybe<Reply>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type Reply = {
  __typename?: 'Reply';
  reviewId?: Maybe<Scalars['ID']>;
  author?: Maybe<Author>;
  value?: Maybe<Scalars['String']>;
};

export type Mutation = {
  __typename?: 'Mutation';
  addReview?: Maybe<AddReviewPayload>;
  addReviewsSeed?: Maybe<AddReviewSeedPayload>;
  addReply?: Maybe<AddReplyPayload>;
  addReaction?: Maybe<AddReactionPayload>;
};


export type MutationAddReviewArgs = {
  input: AddReviewInput;
};


export type MutationAddReviewsSeedArgs = {
  input: AddReviewSeedInput;
};


export type MutationAddReplyArgs = {
  input: AddReplyInput;
};


export type MutationAddReactionArgs = {
  input: AddReactionInput;
};

export type AddReviewPayload = {
  __typename?: 'AddReviewPayload';
  review?: Maybe<Review>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type AddReviewInput = {
  authorName: Scalars['String'];
  authorPhoto?: Maybe<Scalars['Upload']>;
  authorLocation?: Maybe<Scalars['String']>;
  authorEmail: Scalars['String'];
  value?: Maybe<Scalars['String']>;
  rating: Scalars['Int'];
  pros?: Maybe<Scalars['String']>;
  cons?: Maybe<Scalars['String']>;
  clientMutationId?: Maybe<Scalars['String']>;
};


export type AddReviewSeedPayload = {
  __typename?: 'AddReviewSeedPayload';
  message?: Maybe<Scalars['String']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type AddReviewSeedInput = {
  count: Scalars['Int'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type AddReplyPayload = {
  __typename?: 'AddReplyPayload';
  message?: Maybe<Scalars['String']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type AddReplyInput = {
  reviewId: Scalars['ID'];
  authorName: Scalars['String'];
  authorPhoto?: Maybe<Scalars['String']>;
  location?: Maybe<Scalars['String']>;
  value?: Maybe<Scalars['String']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type AddReactionPayload = {
  __typename?: 'AddReactionPayload';
  message?: Maybe<Scalars['String']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type AddReactionInput = {
  reviewId: Scalars['ID'];
  reaction: Reactions;
  clientMutationId?: Maybe<Scalars['String']>;
};

export enum Reactions {
  Like = 'LIKE',
  Dislike = 'DISLIKE'
}

export type AddReviewMutationVariables = Exact<{
  review: AddReviewInput;
}>;


export type AddReviewMutation = (
  { __typename?: 'Mutation' }
  & { addReview?: Maybe<(
    { __typename?: 'AddReviewPayload' }
    & { review?: Maybe<(
      { __typename?: 'Review' }
      & Pick<Review, 'cons' | 'date' | 'pros' | 'value' | 'rating'>
      & { author: (
        { __typename?: 'Author' }
        & Pick<Author, 'name'>
        & { photo?: Maybe<(
          { __typename?: 'Image' }
          & Pick<Image, 'url'>
        )> }
      ) }
    )> }
  )> }
);

export type GetReviewsQueryVariables = Exact<{ [key: string]: never; }>;


export type GetReviewsQuery = (
  { __typename?: 'Query' }
  & { reviews: (
    { __typename?: 'ReviewConnection' }
    & { edges?: Maybe<Array<Maybe<(
      { __typename?: 'ReviewEdge' }
      & { node?: Maybe<(
        { __typename?: 'Review' }
        & Pick<Review, 'id' | 'date' | 'cons' | 'pros' | 'value' | 'rating'>
        & { author: (
          { __typename?: 'Author' }
          & Pick<Author, 'name' | 'location'>
          & { photo?: Maybe<(
            { __typename?: 'Image' }
            & Pick<Image, 'url'>
          )> }
        ) }
      )> }
    )>>> }
  ) }
);


export const AddReviewDocument = gql`
    mutation addReview($review: AddReviewInput!) {
  addReview(input: $review) {
    review {
      author {
        name
        photo {
          url
        }
      }
      cons
      date
      pros
      value
      rating
    }
  }
}
    `;
export type AddReviewMutationFn = ApolloReactCommon.MutationFunction<AddReviewMutation, AddReviewMutationVariables>;

/**
 * __useAddReviewMutation__
 *
 * To run a mutation, you first call `useAddReviewMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useAddReviewMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [addReviewMutation, { data, loading, error }] = useAddReviewMutation({
 *   variables: {
 *      review: // value for 'review'
 *   },
 * });
 */
export function useAddReviewMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<AddReviewMutation, AddReviewMutationVariables>) {
        return ApolloReactHooks.useMutation<AddReviewMutation, AddReviewMutationVariables>(AddReviewDocument, baseOptions);
      }
export type AddReviewMutationHookResult = ReturnType<typeof useAddReviewMutation>;
export type AddReviewMutationResult = ApolloReactCommon.MutationResult<AddReviewMutation>;
export type AddReviewMutationOptions = ApolloReactCommon.BaseMutationOptions<AddReviewMutation, AddReviewMutationVariables>;
export const GetReviewsDocument = gql`
    query getReviews {
  reviews(first: 1) {
    edges {
      node {
        id
        author {
          name
          location
          photo {
            url
          }
        }
        date
        cons
        pros
        value
        rating
      }
    }
  }
}
    `;

/**
 * __useGetReviewsQuery__
 *
 * To run a query within a React component, call `useGetReviewsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetReviewsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetReviewsQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetReviewsQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<GetReviewsQuery, GetReviewsQueryVariables>) {
        return ApolloReactHooks.useQuery<GetReviewsQuery, GetReviewsQueryVariables>(GetReviewsDocument, baseOptions);
      }
export function useGetReviewsLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetReviewsQuery, GetReviewsQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<GetReviewsQuery, GetReviewsQueryVariables>(GetReviewsDocument, baseOptions);
        }
export type GetReviewsQueryHookResult = ReturnType<typeof useGetReviewsQuery>;
export type GetReviewsLazyQueryHookResult = ReturnType<typeof useGetReviewsLazyQuery>;
export type GetReviewsQueryResult = ApolloReactCommon.QueryResult<GetReviewsQuery, GetReviewsQueryVariables>;