import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { ApolloClient, ApolloProvider, InMemoryCache } from '@apollo/client';

const roboto = document.createElement('link');
roboto.href =
    'https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500;700&display=swap';
roboto.rel = 'stylesheet';

document.head.appendChild(roboto);

const client = new ApolloClient({
    uri: 'http://localhost:3000/api/graphql',
    cache: new InMemoryCache(),
});

ReactDOM.render(
    <React.StrictMode>
        <ApolloProvider client={client}>
            <App />
        </ApolloProvider>
    </React.StrictMode>,
    document.getElementById('root'),
);
