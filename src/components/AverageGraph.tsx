import React from 'react';
import BlockLabel from './BlockLabel';
import styled from 'styled-components';
import DonutGraph from './DonutGraph';

const Wrapper = styled.div`
    flex: 1;
`;

const Container = styled.div`
    display: flex;
    flex: 1;
    align-items: center;
`;

const Label = styled.span`
    font-size: 16px;
    color: #696969;
`;

const Number = styled.span`
    font-weight: bold;
    color: #333333;
`;

const AverageGraph = () => {
    return (
        <Wrapper>
            <BlockLabel title={'Средний рейтинг'} />
            <Container>
                <DonutGraph fill={73} />
                <Label>
                    На основе
                    <br />
                    <Number>322</Number> отзывов
                </Label>
            </Container>
        </Wrapper>
    );
};

export default AverageGraph;
