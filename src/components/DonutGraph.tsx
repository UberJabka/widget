import React from 'react';
import COLORS from '../constants/colors';
import styled from 'styled-components';

type Props = {
    size?: number;
    fill: number;
};

const Graph = styled.svg<{ size: number }>`
    width: ${({ size }) => size}px;
    height: ${({ size }) => size}px;
`;

const TextValue = styled.text`
    font-size: 14px;
    font-weight: bold;
    transform: translateY(5px);
    text-anchor: middle;
`;

const DonutGraph: React.FC<Props> = ({ size = 120, fill }) => {
    return (
        <Graph size={size} viewBox="0 0 42 42">
            <circle
                className="donut-hole"
                cx="21"
                cy="21"
                r="15.91549430918954"
                fill="#fff"
            />
            <circle
                className="donut-segment"
                cx="21"
                cy="21"
                r="15.91549430918954"
                fill="transparent"
                stroke={'#fff'}
                strokeWidth={4.3}
            />
            <circle
                className="donut-segment"
                cx="21"
                cy="21"
                r="15.91549430918954"
                fill="transparent"
                stroke={COLORS.GREEN}
                strokeDasharray={`${fill} ${100 - fill}`}
                strokeDashoffset={25}
                strokeWidth={4.3}
            />
            <TextValue x={'50%'} y={'50%'}>
                4.5
            </TextValue>
        </Graph>
    );
};

export default DonutGraph;
