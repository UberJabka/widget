import React from 'react';
import styled from 'styled-components';

type Props = {
    title: string;
};

const Label = styled.p`
    font-size: 18px;
    color: #9ba0b9;
`;

const BlockLabel: React.FC<Props> = ({ title }) => {
    return <Label>{title}</Label>;
};

export default BlockLabel;
