import styled from 'styled-components';

const Slider = styled.div`
    display: flex;
    overflow-x: auto;
`;

export default Slider;
