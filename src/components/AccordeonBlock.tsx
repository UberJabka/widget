import React, { ReactElement, useEffect, useRef, useState } from 'react';
import styled from 'styled-components';

type Props = {
    trigger?: ReactElement;
    active: boolean;
};

const Content = styled.div<{ expandHeight?: number }>`
    overflow: hidden;
    transition: max-height 0.3s ease;
    max-height: ${(props) => props.expandHeight}px;
`;

const AccordeonBlock: React.FC<Props> = ({ active, trigger, children }) => {
    const [maxHeight, setMaxHeight] = useState<number>(0);
    const childRef = useRef<HTMLDivElement>(null);

    useEffect(() => {
        childRef.current && setMaxHeight(childRef.current.scrollHeight);
    }, [children]);

    return (
        <div>
            <div>{trigger}</div>
            <Content ref={childRef} expandHeight={active ? maxHeight : 0}>
                {children}
            </Content>
        </div>
    );
};

export default AccordeonBlock;
