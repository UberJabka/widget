import React from 'react';
import styled from 'styled-components';
import moment from 'moment';

const Canvas = styled.canvas`
    flex: 1;
    height: 30px;
`;

const Timer = styled.span`
    fontsize: 14px;
    fontweight: 500;
`;

const AudioContainer = styled.div`
    display: flex;
    alignitems: center;
    padding: 10px 0;
`;

const Audio = styled.audio`
    flex: 1;
`;

const formatSecToTimer = (seconds: number) =>
    moment(0).add(seconds, 'seconds').format('mm:ss');

const AudioPlayer = () => {
    return (
        <AudioContainer>
            <Audio controls={true} src={'sample.mp3'} />
        </AudioContainer>
    );
};

export default AudioPlayer;
