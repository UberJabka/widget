import React from 'react';
import Review from './Review';
import { useGetReviewsQuery } from '../../graphql/generated/graphql';

const ReviewsList = () => {
    const { data, loading, error } = useGetReviewsQuery();
    if (loading) {
        return <div>Загрузка</div>;
    }
    return (
        <div>
            {data?.reviews.edges?.map(
                (value) =>
                    value?.node && (
                        <Review key={value?.node?.id} node={value.node} />
                    ),
            )}
        </div>
    );
};

export default ReviewsList;
