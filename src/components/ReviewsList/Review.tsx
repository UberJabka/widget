import React, { ChangeEvent, useState } from 'react';
import Author from '../Author';
import Stars from '../Stars';
import ReviewBlock from './ReviewBlock';
import { ReactComponent as Reply } from '../../assets/icons/reply.svg';
import { ReactComponent as Camera } from '../../assets/icons/camera.svg';
import { ReactComponent as ThumbsUp } from '../../assets/icons/thumbs-up.svg';
import { ReactComponent as ThumbsDown } from '../../assets/icons/thumbs-down.svg';
import TextInput from '../TextInput';
import Button from '../Button';
import AccordeonBlock from '../AccordeonBlock';
import ButtonsBlock from '../ButtonsBlock';
import FileInput from '../FileInput';
import FilePreview from '../FilePreview';
import AudioPlayer from '../AudioPlayer';
import styled from 'styled-components';
import Slider from '../Slider';
import { Review as ReviewType } from '../../graphql/generated/graphql';

type Props = {
    node: ReviewType;
};

const Container = styled.div`
    margin: 30px 0;
    width: 100%;
    overflow: hidden;
`;

const Replies = styled.div`
    display: flex;
    justify-content: space-between;
    color: #1d213a;
    font-weight: 500;
`;

const ReplyButton = styled.div`
    display: flex;
    align-items: center;
    margin: 0 3px;
`;
const EmotionButton = styled.div`
    display: flex;
    align-items: center;
    margin-left: 12px;
    margin-right: 3px;
`;
const ReplyWrap = styled.div`
    margin-left: 6px;
    display: inline-block;
`;

const ThumbsContainer = styled.div`
    display: flex;
`;

const Review: React.FC<Props> = ({ node }) => {
    const [formVisible, setFormVisible] = useState<boolean>(false);
    const [files, setFiles] = useState<Array<File>>([]);

    const onAddFile = ({ target }: ChangeEvent<HTMLInputElement>) => {
        if (target.files?.length) {
            const newFiles = [...Array.from(target.files)];
            setFiles((prevState) => {
                return [...prevState, ...newFiles];
            });
        }
    };

    const onRemoveFile = (index: number) => {
        setFiles((prevState) => {
            const newFiles = [...prevState];
            newFiles.splice(index, 1);
            return newFiles;
        });
    };
    return (
        <Container>
            <Author data={node.author} />
            <Stars rating={node.rating} label={'3 месяца назад'} />
            <>
                <Slider>
                    <FilePreview
                        source={
                            'https://www.holland.com/upload_mm/6/2/2/68946_fullimage_kasteel-de-haar-1360.jpg'
                        }
                    />
                    <FilePreview
                        source={
                            'https://www.holland.com/upload_mm/6/2/2/68946_fullimage_kasteel-de-haar-1360.jpg'
                        }
                    />
                    <FilePreview
                        source={
                            'https://www.holland.com/upload_mm/6/2/2/68946_fullimage_kasteel-de-haar-1360.jpg'
                        }
                    />
                </Slider>
                {node.pros && (
                    <ReviewBlock title={'Достоинства'} text={node.pros} />
                )}
                {node.cons && (
                    <ReviewBlock title={'Недостатки'} text={node.cons} />
                )}
                {node.value && (
                    <ReviewBlock title={'Комментарий'} text={node.value} />
                )}
                <AudioPlayer />
            </>
            <Replies>
                <ReplyButton
                    onClick={() => setFormVisible((prevState) => !prevState)}>
                    Ответить{' '}
                    <ReplyWrap>
                        <Reply width={12} />
                    </ReplyWrap>
                </ReplyButton>
                <ThumbsContainer>
                    <EmotionButton>
                        <ThumbsUp width={17} />
                        &nbsp;0
                    </EmotionButton>
                    <EmotionButton>
                        <ThumbsDown width={17} fill={'#9BA0B9'} />
                        &nbsp;5
                    </EmotionButton>
                </ThumbsContainer>
            </Replies>
            <AccordeonBlock active={formVisible}>
                <TextInput type={'textarea'} placeholder={'Комментарий'} />
                <ButtonsBlock>
                    <FileInput onChange={onAddFile}>
                        <Button
                            action={() => {}}
                            type={'icon'}
                            bgColor={'#F2F4FB'}>
                            <Camera width={24} height={24} fill={'#9AB0B9'} />
                        </Button>
                    </FileInput>
                    <Button action={() => {}}>Опубликовать</Button>
                </ButtonsBlock>
                <ButtonsBlock>
                    {files.map((value, index) => (
                        <FilePreview
                            source={URL.createObjectURL(value)}
                            remove={() => onRemoveFile(index)}
                            key={index}
                        />
                    ))}
                </ButtonsBlock>
            </AccordeonBlock>
        </Container>
    );
};

export default Review;
