import React from 'react';
import styled from 'styled-components';

type Props = {
    title: string;
    text?: string | null;
};

const Container = styled.div`
    display: flex;
    flex-direction: column;
    font-size: 15px;
    line-height: 24px;
    margin: 10px 0 16px;
    color: #1d213a;
    font-weight: 300;
`;

const Header = styled.div`
    font-weight: 500;
    margin-bottom: 3px;
`;

const ReviewBlock: React.FC<Props> = ({ title, text }) => {
    return (
        <Container>
            <Header>{title}</Header>
            <div>{text}</div>
        </Container>
    );
};

export default ReviewBlock;
