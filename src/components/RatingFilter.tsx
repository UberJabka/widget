import React from 'react';
import BlockLabel from './BlockLabel';
import Stars from './Stars';

const RatingsFilter = () => {
    return (
        <div>
            <BlockLabel title={'Показать отзывы с оценкой:'} />
            <div>
                <Stars rating={5} label={'8776 отзывов'} />
                <Stars rating={4} label={'3459 отзывов'} />
                <Stars rating={3} label={'1801 отзывов'} />
                <Stars rating={2} label={'91 отзыв'} />
                <Stars rating={1} label={'101 отзыв'} />
            </div>
        </div>
    );
};

export default RatingsFilter;
