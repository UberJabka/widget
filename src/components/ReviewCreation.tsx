import React, { ChangeEvent, useState } from 'react';
import { useMediaQuery } from 'react-responsive';
import Button from './Button';
import BlockLabel from './BlockLabel';
import { ReactComponent as Cross } from '../assets/icons/times.svg';
import { ReactComponent as Mic } from '../assets/icons/microphone.svg';
import { ReactComponent as Camera } from '../assets/icons/camera.svg';
import AccordeonBlock from './AccordeonBlock';
import ButtonsBlock from './ButtonsBlock';
import TextInput from './TextInput';
import FileInput from './FileInput';
import FilePreview from './FilePreview';
import styled from 'styled-components';
import Stars from './Stars';
import { ReactComponent as Vk } from '../assets/icons/vk.svg';
import { ReactComponent as Fb } from '../assets/icons/facebook.svg';
import { ReactComponent as Google } from '../assets/icons/google.svg';

const ReviewCreation = () => {
    return <></>;
};

export default ReviewCreation;
