import React from 'react';
import styled from 'styled-components';

type Props = {
    action: () => void;
    type?: 'outline' | 'cancel' | 'icon';
    bgColor?: string;
};

const ButtonContainer = styled.div<{ bgColor?: string }>`
    display: flex;
    flex: 1;
    justify-content: center;
    align-items: center;
    background-color: ${(props) => props.bgColor || '#651fff'};
    border: #651fff 2px solid;
    height: 40px;
    border-radius: 5px;
    color: #ffffff;
    margin: 7px 0;
    cursor: pointer;
`;

const OutlinedButton = styled(ButtonContainer)`
    background-color: #ffffff;
    color: #651fff;
`;

const CancelButton = styled(ButtonContainer)`
    border: none;
    background-color: #ffffff;
    color: #1d213a;
    box-shadow: 0px 2px 10px rgba(89, 31, 255, 0.07);
`;

const IconButton = styled(ButtonContainer)`
    width: 50px;
    height: 50px;
    flex: none;
    align-items: center;
    padding: 0;
    border: 0;
`;

const Button: React.FC<Props> = ({ children, action, type, bgColor }) => {
    switch (type) {
        case 'cancel':
            return <CancelButton onClick={action}>{children}</CancelButton>;
        case 'icon':
            return (
                <IconButton onClick={action} bgColor={bgColor}>
                    {children}
                </IconButton>
            );
        case 'outline':
            return (
                <OutlinedButton onClick={action} bgColor={bgColor}>
                    {children}
                </OutlinedButton>
            );
        default:
            return (
                <ButtonContainer bgColor={bgColor} onClick={action}>
                    {children}
                </ButtonContainer>
            );
    }
};

export default Button;
