import React from 'react';
import { ReactComponent as Star } from '../assets/icons/star.svg';
import styled from 'styled-components';

type Props = {
    rating: number;
    label?: string;
};

const Container = styled.div`
    margin: 8px 0;
    display: flex;
    align-items: center;
`;

const StarLabel = styled.label`
    margin: 0 4px;
    & > input {
        display: none;
    }
`;

const StarIcon = styled(Star)<{ active?: boolean }>`
    width: 14px;
    height: 14px;
    @media (max-width: 991px) {
        width: 22px;
        height: 22px;
    }
    fill: ${({ active }) => (active ? '#FFD537' : '#E2E4EE')};
`;

const Label = styled.span`
    font-size: 16px;
    font-weight: normal;
    margin: 0 5px;
    color: #696969;
`;

const Stars: React.FC<Props> = ({ rating, label }) => {
    const inputs = new Array(5).fill(0);

    return (
        <Container>
            {inputs.map((value, index) => (
                <StarLabel>
                    <input
                        key={index}
                        type={'radio'}
                        checked={index + 1 === rating}
                        value={index + 1}
                        disabled={true}
                    />
                    <span>
                        <StarIcon active={index < rating} />
                    </span>
                </StarLabel>
            ))}
            <Label>{label}</Label>
        </Container>
    );
};

export default Stars;
