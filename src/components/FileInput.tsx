import React, { ChangeEvent } from 'react';
import styled from 'styled-components';

type Props = {
    onChange: (event: ChangeEvent<HTMLInputElement>) => void;
};

const Input = styled.input`
    display: none;
`;

const FileInput: React.FC<Props> = ({ onChange, children }) => {
    return (
        <label>
            <Input
                value={''}
                onChange={onChange}
                type={'file'}
                accept={'image/*'}
                multiple={true}
            />
            <span>{children}</span>
        </label>
    );
};

export default FileInput;
