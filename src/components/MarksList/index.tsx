import React from 'react';
import BlockLabel from '../BlockLabel';
import Mark from './Mark';
import styled from 'styled-components';

const Wrapper = styled.div`
    flex: 1;
`;

const BlockList = styled.div`
    display: flex;
    flex-wrap: wrap;
`;

const MarksList = () => {
    return (
        <Wrapper>
            <BlockLabel title={'Метки'} />
            <BlockList>
                <Mark active={true}>Все отзывы</Mark>
                <Mark>#Тупое</Mark>
                <Mark>#Говно</Mark>
                <Mark>#Тупого</Mark>
                <Mark>#Говна</Mark>
                <Mark>#Гоблача</Mark>
                <Mark>#Гоблак беги</Mark>
                <Mark>#Все корабль все в ...</Mark>
                <Mark>...</Mark>
            </BlockList>
        </Wrapper>
    );
};

export default MarksList;
