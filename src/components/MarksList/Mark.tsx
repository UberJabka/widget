import styled from 'styled-components';

const Mark = styled.div<{ active?: boolean }>`
    display: flex;
    flex-basis: auto;
    padding: 12px 18px;
    white-space: nowrap;
    font-size: 14px;
    color: ${({ active }) => (active ? '#FFFFFF' : '#9ba0b9')};
    background-color: ${({ active }) => (active ? '#725BFF' : '#f2f4fb')};
    margin: 5px 5px;
    border-radius: 5px;
`;

export default Mark;
