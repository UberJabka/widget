import React from 'react';
import { ReactComponent as Cross } from '../assets/icons/times.svg';
import styled from 'styled-components';

type Props = {
    source: string;
    isExpanded?: boolean;
    remove?: () => void;
};

const Container = styled.div`
    position: relative;
    margin: 14px 5px;
`;

const Image = styled.img<{ expanded?: boolean }>`
    width: ${({ expanded }) => (expanded ? '300px' : '50px')};
    height: ${({ expanded }) => (expanded ? '150px' : '50px')};
    border-radius: 5px;
`;

const CrossContainer = styled.span`
    display: flex;
    align-items: center;
    justify-content: center;
    width: 20px;
    height: 20px;
    border-radius: 50%;
    background-color: #000000;
    position: absolute;
    top: -9px;
    right: -9px;
`;

const CrossIcon = styled(Cross)`
    fill: #ffffff;
    width: 12px;
    height: 12px;
`;

const FilePreview: React.FC<Props> = ({ source, isExpanded, remove }) => {
    return (
        <Container>
            <Image expanded={isExpanded} src={source} />
            {remove && (
                <CrossContainer onClick={remove}>
                    <CrossIcon />
                </CrossContainer>
            )}
        </Container>
    );
};

export default FilePreview;
