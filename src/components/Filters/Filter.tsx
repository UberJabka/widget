import React from 'react';
import AccordeonBlock from '../AccordeonBlock';
import styled from 'styled-components';

type Props = {
    active: boolean;
    options: {
        label: string;
        action: () => void;
        disable?: boolean;
        reset?: boolean;
    }[];
};

const Container = styled.div`
    color: #1d213a;
    font-weight: 500;
    font-size: 15px;
    @media (min-width: 992px) {
        position: absolute;
        top: 25px;
        background: white;
        padding: 0 20px;
        left: -20px;
    }
`;

const Option = styled.div<{ disable?: boolean; reset?: boolean }>`
    margin: 18px 0;
    color: ${({ disable, reset }) =>
        (disable && '#D9DDEA') || (reset && '#F44336')};
    font-weight: ${({ reset }) => reset && 'normal'};
`;

const Filter: React.FC<Props> = ({ active, options }) => {
    return (
        <Container>
            <AccordeonBlock active={active}>
                {options.map((value, index) => (
                    <Option
                        key={index}
                        disable={value.disable}
                        reset={value.reset}
                        onClick={value.action}>
                        {value.label}
                    </Option>
                ))}
            </AccordeonBlock>
        </Container>
    );
};

export default Filter;
