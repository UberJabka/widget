import React, { useState } from 'react';
import Filter from './Filter';
import { ReactComponent as Arrow } from '../../assets/icons/arrow.svg';
import styled from 'styled-components';
import { useMediaQuery } from 'react-responsive';

const Container = styled.div`
    padding: 16px 0;
    width: 100%;
    order: 2;
`;

const Triggers = styled.div`
    display: flex;
    justify-content: space-between;
`;

const Trigger = styled.div`
    display: flex;
    align-items: center;
    @media (min-width: 992px) {
        position: relative;
    }
`;

const ArrowIcon = styled(Arrow)<{ active: boolean }>`
    width: 10px;
    height: 17px;
    margin-left: 5px;
    transition: transform 0.3s ease;
    transform: ${({ active }) =>
        `rotateX(${active ? '180' : '0'}deg) rotateZ(0deg)`};
`;

const Label = styled.span`
    color: #9ba0b9;
`;

const Filters = () => {
    const [activeFilters, setActiveFilters] = useState<Array<boolean>>(
        new Array(2).fill(false),
    );

    const isDesktop = useMediaQuery({ minWidth: 992 });

    const triggerActiveFilter = (index: number) => {
        setActiveFilters((prevState) => {
            const newArray = [...prevState];
            newArray.fill(false);
            newArray[index] = !prevState[index];
            return newArray;
        });
    };

    return (
        <Container>
            <Triggers>
                <Trigger onClick={() => triggerActiveFilter(0)}>
                    Все отзывы
                    <ArrowIcon active={activeFilters[0]} />
                    {isDesktop && (
                        <Filter
                            active={activeFilters[0]}
                            options={[
                                { label: 'Только с фото', action: () => null },
                                {
                                    label: 'Только с видео',
                                    action: () => null,
                                    disable: true,
                                },
                                {
                                    label: 'Только с аудиоотзывом',
                                    action: () => null,
                                },
                                {
                                    label: 'Сбросить все фильтры',
                                    action: () => null,
                                    reset: true,
                                },
                            ]}
                        />
                    )}
                </Trigger>
                <Trigger onClick={() => triggerActiveFilter(1)}>
                    <Label>Сортировать по:&nbsp;</Label>
                    Дате
                    <ArrowIcon active={activeFilters[1]} />
                    {isDesktop && (
                        <Filter
                            active={activeFilters[1]}
                            options={[
                                {
                                    label: 'Рейтингу',
                                    action: () => null,
                                },
                                {
                                    label: 'ХЗ ЧЕМУ САМИ ПРИДУМАЙТЕ',
                                    action: () => null,
                                },
                                {
                                    label: 'Я МАССИВ МНЕ НАСРАТЬ',
                                    action: () => null,
                                },
                            ]}
                        />
                    )}
                </Trigger>
            </Triggers>
            {!isDesktop && (
                <>
                    <Filter
                        active={activeFilters[0]}
                        options={[
                            { label: 'Только с фото', action: () => null },
                            {
                                label: 'Только с видео',
                                action: () => null,
                                disable: true,
                            },
                            {
                                label: 'Только с аудиоотзывом',
                                action: () => null,
                            },
                            {
                                label: 'Сбросить все фильтры',
                                action: () => null,
                                reset: true,
                            },
                        ]}
                    />
                    <Filter
                        active={activeFilters[1]}
                        options={[
                            {
                                label: 'Рейтингу',
                                action: () => null,
                            },
                            {
                                label: 'ХЗ ЧЕМУ САМИ ПРИДУМАЙТЕ',
                                action: () => null,
                            },
                            {
                                label: 'Я МАССИВ МНЕ НАСРАТЬ',
                                action: () => null,
                            },
                        ]}
                    />
                </>
            )}
        </Container>
    );
};

export default Filters;
