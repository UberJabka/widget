import React from 'react';
import styled from 'styled-components';
import { Author as AuthorType } from '../graphql/generated/graphql';
import { Maybe } from 'graphql/jsutils/Maybe';

type Props = {
    data: AuthorType;
};

const Container = styled.div`
    display: flex;
    flex: 1;
    align-items: center;
`;

const Photo = styled.img`
    width: 50px;
    height: 50px;
    border-radius: 50%;
`;

const Text = styled.div`
    display: flex;
    flex-direction: column;
    margin-left: 13px;
`;

const Name = styled.span`
    color: #1d213a;
    font-size: 15px;
    font-weight: 500;
`;

const Location = styled.span`
    font-size: 13px;
    color: #696969;
    margin-top: 3px;
`;

const Author: React.FC<Props> = ({ data }) => {
    return (
        <Container>
            <Photo src={data?.photo?.url} />
            <Text>
                <Name>{data?.name}</Name>
                <Location>{data?.location}</Location>
            </Text>
        </Container>
    );
};

export default Author;
