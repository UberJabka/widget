import React, { useState } from 'react';
import styled from 'styled-components';

type Props = {
    placeholder: string;
    type: 'input' | 'textarea';
};

const Container = styled.div`
    position: relative;
    font-size: 15px;
    display: flex;
    margin: 18px 0;
    flex: 1;
`;

const Input = styled.input`
    border: 1px solid #e2e4ee;
    border-radius: 5px;
    padding: 16px 14px;
    color: #646879;
    font-size: 15px;
    flex: 1;
    &:focus {
        outline: none;
    }
`;

const TextArea = styled(Input)`
    min-height: 141px;
    resize: none;
`;

const Placeholder = styled.span<{ active: boolean }>`
    position: absolute;
    left: 14px;
    top: 18px;
    padding: 0 5px;
    background: #ffffff;
    color: #9ba0b9;
    transition: transform 0.3s ease, font-size 0.5s ease;
    transform: ${({ active }) =>
        active && 'translateY(-24px) translateX(-5px)'};
    font-size: ${({ active }) => active && '12px'};
`;

const TextInput: React.FC<
    Props &
        React.HTMLAttributes<HTMLInputElement> &
        React.HTMLAttributes<HTMLTextAreaElement>
> = ({ placeholder, type, ...props }) => {
    const [isFocused, setFocused] = useState<boolean>(false);

    return (
        <Container>
            {(type === 'input' && (
                <Input
                    onFocus={() => setFocused(true)}
                    onBlur={(event) => setFocused(!!event.target.value)}
                    {...props}
                />
            )) ||
                (type === 'textarea' && (
                    <TextArea
                        onFocus={() => setFocused(true)}
                        onBlur={(event) => setFocused(!!event.target.value)}
                        {...props}
                    />
                ))}
            <Placeholder active={isFocused}>{placeholder}</Placeholder>
        </Container>
    );
};

export default TextInput;
