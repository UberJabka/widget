import { useFormik } from 'formik';
import Stars from './Stars';
import ButtonsBlock from './ButtonsBlock';
import Button from './Button';
import { ReactComponent as Vk } from '../assets/icons/vk.svg';
import { ReactComponent as Fb } from '../assets/icons/facebook.svg';
import { ReactComponent as Google } from '../assets/icons/google.svg';
import TextInput from './TextInput';
import React from 'react';
import styled from 'styled-components';
import { useAddReviewMutation } from '../graphql/generated/graphql';

const SocialsContainer = styled.div`
    display: flex;
    align-items: center;
`;

const MainInputs = styled.div`
    display: flex;
    flex-wrap: wrap;
`;

const Hint = styled.span`
    flex: 1;
    text-align: center;
    font-size: 12px;
    text-transform: uppercase;
    color: #9ba0b9;
    font-weight: bold;
`;

const Form = () => {
    const addReviewForm = useFormik({
        initialValues: {
            rating: 0,
            email: '',
            name: '',
            pros: '',
            cons: '',
            review: '',
            images: [],
            record: null,
        },
        onSubmit: (values) => {
            addReview({
                variables: {
                    review: {
                        authorEmail: values.email,
                        authorName: values.name,
                        value: values.review,
                        cons: values.cons,
                        pros: values.pros,
                        rating: values.rating,
                    },
                },
            });
        },
    });

    const [
        addReview,
        { data, loading, error, client },
    ] = useAddReviewMutation();

    return (
        <div>
            <div>Новый отзыв</div>
            <form>
                <Stars rating={1} label={'Хороший товар'} />
                <MainInputs>
                    <SocialsContainer>
                        <ButtonsBlock>
                            <Button
                                type={'icon'}
                                action={() => {}}
                                bgColor={'#4A76A8'}>
                                <Vk height={23} fill={'#FFFFFF'} />
                            </Button>
                            <Button
                                type={'icon'}
                                action={() => {}}
                                bgColor={'#4267B2'}>
                                <Fb height={23} fill={'#FFFFFF'} />
                            </Button>
                            <Button
                                type={'icon'}
                                action={() => {}}
                                bgColor={'#4267B2'}>
                                <Google height={23} fill={'#FFFFFF'} />
                            </Button>
                        </ButtonsBlock>
                        <Hint>или укажите email</Hint>
                    </SocialsContainer>
                    <TextInput placeholder={'Email'} type={'input'} />
                    <TextInput placeholder={'Имя Фамилия'} type={'input'} />
                    <TextInput placeholder={'Плюсы'} type={'input'} />
                    <TextInput placeholder={'Минусы'} type={'input'} />
                    <TextInput placeholder={'Комментарий'} type={'textarea'} />
                    <Button action={addReviewForm.submitForm}>
                        Опубликовать
                    </Button>
                </MainInputs>
            </form>
        </div>
    );
};

export default Form;
