/**
 * Function of linear interpolation for array
 * @param data - Array to interpolate
 * @param fitCount - Count of resulting array
 * @return new interpolated array
 */
const interpolateArray = (data: number[], fitCount: number) => {
    const linearInterpolate = (
        before: number,
        after: number,
        atPoint: number,
    ) => {
        return before + (after - before) * atPoint;
    };

    const newData = [];
    const springFactor = (data.length - 1) / (fitCount - 1);
    newData[0] = data[0];
    for (let i = 1; i < fitCount - 1; i++) {
        const tmp = i * springFactor;
        const before = Number(Math.floor(tmp).toFixed());
        const after = Number(Math.ceil(tmp).toFixed());
        const atPoint = tmp - before;
        newData[i] = linearInterpolate(data[before], data[after], atPoint);
    }
    newData[fitCount - 1] = data[data.length - 1];

    return newData;
};

export default interpolateArray;
