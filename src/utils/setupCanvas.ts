/**
 * Function rescales canvas according to css size and pixel ratio
 * @param canvas
 * @param dpr Device pixel ratio
 * @return 2d canvas context
 */
const setupCanvas = (canvas: HTMLCanvasElement | null, dpr = 1) => {
    if (!canvas) return null;
    const rect = canvas.getBoundingClientRect();
    canvas.width = rect.width * dpr;
    canvas.height = rect.height * dpr;

    const ctx = canvas.getContext('2d');
    ctx?.scale(dpr, dpr);
    return ctx;
};

export default setupCanvas;
