const COLORS = {
    GREEN: '#46E9A4',
    BLACK: '#333333',
    YELLOW: '#FFD537',
    BLUE_GRAY: '#9BA0B9',
    DARK_GRAY: '#696969',
    RED: '#F44336',
};

export default COLORS;
