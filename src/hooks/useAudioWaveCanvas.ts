import { RefObject, useEffect, useRef, useState } from 'react';
import setupCanvas from '../utils/setupCanvas';
import interpolateArray from '../utils/interpolateArray';
import { IHowlerState } from './useHowler';
//
// type Config = {
//     lineWidth: number;
//     primaryColor: string;
//     secondaryColor: string;
//     padding: number;
//     gap: number;
// };
//
// /**
//  * Device pixel ratio
//  */
// const dpr = window.devicePixelRatio || 1;
//
// const useAudioWaveCanvas = (
//     wave: number[],
//     playerState: IHowlerState,
//     config: Config,
// ): [RefObject<HTMLCanvasElement>] => {
//     const canvasRef = useRef<HTMLCanvasElement>(null);
//     const ctxRef = useRef<CanvasRenderingContext2D>(
//         setupCanvas(canvasRef.current, dpr),
//     );
//     const positionRef = useRef<number>(0);
//     const timerRef = useRef(0);
//
//     const sceneInit = () => {
//         sceneInitHandlers();
//     };
//
//     const sceneInitHandlers = () => {
//         window.addEventListener('resize', () => {});
//     };
//
//     const drawLineSegment = (
//         ctx: CanvasRenderingContext2D,
//         x: number,
//         height: number,
//         width: number,
//         value: number,
//         color: string,
//     ) => {
//         const normalizedValue = 0.5 - value / 2;
//         const upperPoint = (normalizedValue * height) / dpr;
//         const lowerPoint = upperPoint + (value * height) / dpr;
//         ctx.lineWidth = width; // how thick the line is
//         ctx.lineCap = 'round';
//         ctx.strokeStyle = color; // what color our line is
//         ctx.beginPath();
//         ctx.moveTo(x, upperPoint);
//         ctx.lineTo(x, lowerPoint);
//         ctx.stroke();
//     };
//
//     useEffect(() => {
//         if (
//             ctxRef &&
//             canvasRef.current &&
//             playerState.duration &&
//             playerState.isReady
//         ) {
//             const { height, width } = canvasRef.current;
//             const {
//                 gap,
//                 lineWidth,
//                 padding,
//                 primaryColor,
//                 secondaryColor,
//             } = config;
//             let x = lineWidth + padding / dpr;
//             const interpolatedWave = interpolateArray(
//                 wave,
//                 (width - padding * 2) / (lineWidth + gap) / dpr,
//             );
//             const barsCount = interpolatedWave.length;
//             const barTick = width / barsCount;
//             const tick = (playerState.duration * 1000 * barTick) / width;
//             const render = () => {
//                 ctx.clearRect(0, 0, width * dpr, height * dpr);
//                 x = padding;
//                 interpolatedWave.forEach((value, index) => {
//                     let color =
//                         barTick * index < positionRef.current
//                             ? primaryColor
//                             : secondaryColor;
//                     drawLineSegment(
//                         ctx,
//                         x,
//                         height,
//                         lineWidth,
//                         value * 0.8,
//                         color,
//                     );
//                     x += lineWidth + gap;
//                 });
//                 positionRef.current += barTick;
//             };
//             if (playerState.isReady && !playerState.isPlaying) render();
//             if (playerState.isPlaying) {
//                 timerRef.current = setInterval(render, tick);
//             }
//             if (playerState.isPaused) clearInterval(timerRef.current);
//
//             if (playerState.isPositionChanged) {
//                 positionRef.current =
//                     (playerState.position * width) / playerState.duration;
//                 render();
//             }
//
//             if (playerState.isStopped) {
//                 clearInterval(timerRef.current);
//                 positionRef.current = 0;
//                 render();
//             }
//             if (playerState.isEnded) {
//                 clearInterval(timerRef.current);
//                 positionRef.current = 0;
//             }
//             return () => {
//                 clearInterval(timerRef.current);
//             };
//         }
//     }, [
//         config,
//         drawLineSegment,
//         playerState.duration,
//         playerState.isEnded,
//         playerState.isPaused,
//         playerState.isPlaying,
//         playerState.isPositionChanged,
//         playerState.isReady,
//         playerState.isStopped,
//         playerState.position,
//         wave,
//     ]);
//
//     return [canvasRef];
// };
//
// export default useAudioWaveCanvas;
