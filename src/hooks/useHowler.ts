import { Howl, HowlOptions } from 'howler';
import { useEffect, useRef, useReducer } from 'react';

export interface IHowlerState {
    isLoading: boolean;
    isReady: boolean;
    isError: boolean;
    isPlaying: boolean;
    isPaused: boolean;
    isStopped: boolean;
    isEnded: boolean;
    isPositionChanged: boolean;
    position: number;
    duration: number;
}

type Action =
    | { type: 'loaded'; payload: number }
    | { type: 'error' }
    | { type: 'play' }
    | { type: 'playing' }
    | { type: 'seek'; payload: number }
    | { type: 'pause' }
    | { type: 'stop' }
    | { type: 'end' };

const initialState: IHowlerState = {
    isLoading: true,
    isReady: false,
    isError: false,
    isPlaying: false,
    isPaused: false,
    isStopped: false,
    isEnded: false,
    isPositionChanged: false,
    position: 0,
    duration: 0,
};

const howlerReducer = (state = initialState, action: Action): IHowlerState => {
    switch (action.type) {
        case 'loaded':
            return {
                ...state,
                isLoading: false,
                isReady: true,
                duration: action.payload,
            };
        case 'error':
            return {
                ...state,
                isLoading: false,
                isError: true,
            };
        case 'play':
            return {
                ...state,
                isPlaying: true,
                isPaused: false,
                isStopped: false,
                isEnded: false,
                isPositionChanged: false,
                position: state.isEnded ? 0 : state.position,
            };
        case 'playing':
            return {
                ...state,
                position:
                    state.position + 0.1 < state.duration
                        ? state.position + 0.1
                        : state.duration,
            };
        case 'pause':
            return {
                ...state,
                isPaused: true,
                isPlaying: false,
                isEnded: false,
            };
        case 'seek':
            return {
                ...state,
                position: action.payload,
                isPositionChanged: true,
            };
        case 'stop':
            return {
                ...state,
                isStopped: true,
                isPaused: false,
                isPlaying: false,
                isEnded: false,
                position: 0,
                isPositionChanged: false,
            };
        case 'end':
            return {
                ...state,
                isPlaying: false,
                isEnded: true,
                isPositionChanged: false,
            };
        default:
            return state;
    }
};

const useHowler = (
    options: HowlOptions,
): [
    IHowlerState,
    {
        play: () => void;
        pause: () => void;
        stop: () => void;
        seek: (time: number) => void;
    },
] => {
    const [state, dispatch] = useReducer(howlerReducer, initialState);

    const soundRef = useRef(new Howl(options));
    const timerRef = useRef(0);

    const play = () => {
        if (state.isReady && !state.isPlaying) soundRef.current.play();
    };

    const pause = () => {
        if (state.isPlaying) soundRef.current.pause();
    };

    const stop = () => {
        if (state.isPaused || state.isPlaying) soundRef.current.stop();
    };

    const seek = (time: number) => {
        soundRef.current.seek(time);
        dispatch({ type: 'seek', payload: time });
    };

    useEffect(() => {
        soundRef.current.on('load', () => {
            dispatch({ type: 'loaded', payload: soundRef.current.duration() });
        });

        soundRef.current.on('play', () => {
            dispatch({ type: 'play' });
            timerRef.current = setInterval(() => {
                dispatch({ type: 'playing' });
            }, 100);
        });

        soundRef.current.on('pause', () => {
            dispatch({ type: 'pause' });
            clearInterval(timerRef.current);
        });

        soundRef.current.on('stop', () => {
            dispatch({ type: 'stop' });
            clearInterval(timerRef.current);
        });

        soundRef.current.on('end', () => {
            dispatch({ type: 'end' });
            clearInterval(timerRef.current);
        });

        return () => {
            soundRef.current.off();
        };
    }, [state.duration, state.position]);

    return [state, { play, pause, stop, seek }];
};

export default useHowler;
